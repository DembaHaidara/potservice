package com.bforbank.cagnotteservice.service;

import com.bforbank.cagnotteservice.exception.EntityNotFoundException;
import com.bforbank.cagnotteservice.entity.Customer;
import com.bforbank.cagnotteservice.entity.Transaction;
import com.bforbank.cagnotteservice.repository.CustomerRepository;
import com.bforbank.cagnotteservice.repository.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceTest {

    @InjectMocks
    private PotService potService;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private TransactionRepository transactionRepository;
    private static final Long CUSTOMER_ID = 1L;
    private static final Double TRANSACTION_AMOUNT = 50.0;

    @Test
    void addTransactionTest() {

        //GIVEN
        Customer customer = new Customer();
        customer.setId(CUSTOMER_ID);
        customer.setName("");
        when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.of(customer));

        //WHEN
        potService.addTransaction(CUSTOMER_ID,TRANSACTION_AMOUNT);

        //THEN
        verify(transactionRepository).save(any(Transaction.class));
    }

    @Test
    void consultBalance(){
        //GIVEN
        Customer customer = new Customer();
        customer.setId(CUSTOMER_ID);
        customer.setName("");
        when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.of(customer));
        List<Transaction> Simulatedtransactions = Arrays.asList(
                new Transaction(50.0),
                new Transaction(-20.0),
                new Transaction(30.0)
        );
        when(transactionRepository.findAllByCustomerId(CUSTOMER_ID)).thenReturn(Simulatedtransactions);
        //WHEN
        Double actualBalance = potService.consulBalance(CUSTOMER_ID);
        //THEN
        Double expectedBalance = 60.0;
        assertEquals(expectedBalance, actualBalance);
    }

    @Test
    void potIsAvailableTest() {
        //GIVEN
        Customer customer = new Customer();
        customer.setId(CUSTOMER_ID);
        customer.setName("");
        when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.of(customer));
        when(transactionRepository.findAllByCustomerId(CUSTOMER_ID)).thenReturn(Arrays.asList(
                new Transaction(50.0),
                new Transaction(-20.0),
                new Transaction(30.0),
                new Transaction(5.0)
        ));
        //WHEN
        boolean potAvailable = potService.isPotAvailable(CUSTOMER_ID);
        //THEN
        assertTrue(potAvailable);
    }

    @Test
    void potIsNotAvailableTest() {
        //GIVEN
        Customer customer = new Customer();
        customer.setId(CUSTOMER_ID);
        customer.setName("");
        when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.of(customer));
        when(transactionRepository.findAllByCustomerId(CUSTOMER_ID)).thenReturn(Arrays.asList(
                new Transaction(5.0),
                new Transaction(2.0)
        ));
        //WHEN
        boolean potNotAvailable = potService.isPotAvailable(CUSTOMER_ID);
        //THEN
        assertFalse(potNotAvailable);
    }

    @Test
    void potIsNotAvailableInsufficientBalanceTest() {
        //GIVEN
        Customer customer = new Customer();
        customer.setId(CUSTOMER_ID);
        customer.setName("");
        when(customerRepository.findById(CUSTOMER_ID)).thenReturn(Optional.of(customer));
        when(transactionRepository.findAllByCustomerId(CUSTOMER_ID)).thenReturn(Arrays.asList(
                new Transaction(5.0),
                new Transaction(2.0),
                new Transaction(1.0)
                ));
        //WHEN
        boolean potNotAvailable = potService.isPotAvailable(CUSTOMER_ID);
        //THEN
        assertFalse(potNotAvailable);
    }
    @Test
    public void customerNotExistException() {

        // GIVEN
        Long noExistingClientId = 99L;
        when(customerRepository.findById(noExistingClientId)).thenReturn(Optional.empty());

        // WHEN & THEN
        EntityNotFoundException thrown = assertThrows(EntityNotFoundException.class, () -> {
            potService.addTransaction(noExistingClientId, 50.0);
        }, "Expected addTransaction to throw, but it didn't");

        assertEquals("Customer not found with ID: " + noExistingClientId, thrown.getMessage());
    }
    @Test
    void testCreateCustomer() {
        //GIVEN
        String testName = "Demba Haidara";
        Customer testCustomer = new Customer();
        testCustomer.setName(testName);
        when(customerRepository.save(any(Customer.class))).thenReturn(testCustomer);

        //WHEN
        Customer createdCustomer = potService.createCustomer(testName);

        //THEN
        assertEquals(testName, createdCustomer.getName());
    }
}
