package com.bforbank.cagnotteservice.controller;

import com.bforbank.cagnotteservice.entity.Customer;
import com.bforbank.cagnotteservice.entity.Transaction;
import com.bforbank.cagnotteservice.repository.CustomerRepository;
import com.bforbank.cagnotteservice.repository.TransactionRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class PotControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private TransactionRepository transactionRepository;

    @BeforeEach
    public void setup() {
        transactionRepository.deleteAll();
        customerRepository.deleteAll();
    }

    @Test
    void testAddTransactionWithPositiveAmount() throws Exception {
        Customer customer = new Customer();
        customer.setName("Thomas Ribeiro");
        customerRepository.save(customer);

        Long clientId = customerRepository.findAll().get(0).getId();
        Double amount = 50.0;
        String content = String.format("{\"clientId\": %d, \"amount\": %.2f}", clientId, amount);

        mockMvc.perform(post("/pots/add-transaction")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isOk());
    }

    @Test
    void testAddTransactionWithNegativeAmount() throws Exception {
        Customer customer = new Customer();
        customer.setName("Thomas Ribeiro");
        customerRepository.save(customer);

        Long clientId = customerRepository.findAll().get(0).getId();
        Double amount = -50.0;
        String content = String.format("{\"clientId\": %d, \"amount\": %.2f}", clientId, amount);

        mockMvc.perform(post("/pots/add-transaction")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(content))
                .andExpect(status().isBadRequest());

    }

    @Test
    void consultBalanceTes() throws Exception {
        Customer customer = new Customer();
        customer.setName("Thomas Ribeiro");
        Customer savedCustomer = customerRepository.save(customer);

        Transaction transaction1 = new Transaction();
        transaction1.setAmount(100.0);
        transaction1.setCustomer(savedCustomer);
        transactionRepository.save(transaction1);

        Long clientId = customerRepository.findAll().get(0).getId();
        mockMvc.perform(get("/pots/consult-balance/"+ clientId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("100.0"));
    }

    @Test
    void potIsAvailableTest() throws Exception {
        Customer customer = new Customer();
        customer.setName("Thomas Ribeiro");
        Customer savedCustomer = customerRepository.save(customer);

        List<Transaction> transactions = List.of(
                new Transaction(100.0,savedCustomer),
                new Transaction(20.0,savedCustomer),
                new Transaction(5.0,savedCustomer)
        );
        transactionRepository.saveAll(transactions);


        mockMvc.perform(get("/pots/is-available/" + savedCustomer.getId())
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("true"));
    }


    @Test
    void potIsNotAvailableTest() throws Exception {
        Customer customer = new Customer();
        customer.setName("Thomas Ribeiro");
        Customer savedCustomer = customerRepository.save(customer);

        Transaction transaction2 = new Transaction();
        transaction2.setAmount(-100.0);
        transaction2.setCustomer(savedCustomer);
        transactionRepository.save(transaction2);

        Long clientId = customerRepository.findAll().get(0).getId();

        mockMvc.perform(get("/pots/is-available/"+clientId)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("false"));
    }


    @Test
    public void addTransactionClientNotFoundTest() throws Exception {
        Long nonExistentClientId = Long.MAX_VALUE;
        Double amount = 50.0;
        String requestBody = String.format("{\"clientId\": %d, \"amount\": %.2f}", nonExistentClientId, amount);

        mockMvc.perform(post("/pots/add-transaction")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(requestBody))
                .andExpect(status().isNotFound());
    }


}
