package com.bforbank.cagnotteservice.entity;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

public class TransactionTest {

    private static final Long TRANSACTION_ID = 1L;
    private static final Double TRANSACTION_AMOUNT = 100.0;
    private static final LocalDateTime TRANSACTION_DATE = LocalDateTime.now();
    private static final Customer CUSTOMER = new Customer();

    @Test
    void testTransactionEntity() {
        // Créer une instance de Transaction
        Transaction transaction = new Transaction();
        transaction.setId(TRANSACTION_ID);
        transaction.setAmount(TRANSACTION_AMOUNT);
        transaction.setDateTransaction(TRANSACTION_DATE);
        transaction.setCustomer(CUSTOMER);

        assertEquals(TRANSACTION_ID, transaction.getId());
        assertEquals(TRANSACTION_AMOUNT, transaction.getAmount());
        assertEquals(TRANSACTION_DATE, transaction.getDateTransaction());
        assertEquals(CUSTOMER, transaction.getCustomer());

        Transaction sameTransaction = new Transaction();
        sameTransaction.setId(TRANSACTION_ID);
        sameTransaction.setAmount(TRANSACTION_AMOUNT);
        sameTransaction.setDateTransaction(TRANSACTION_DATE);
        sameTransaction.setCustomer(CUSTOMER);
        assertEquals(transaction, sameTransaction);

        assertEquals(transaction.hashCode(), sameTransaction.hashCode());

        // Vérifier la méthode toString
        String expectedToString = "Transaction{id=1, amount=100.0, dateTransaction=" + TRANSACTION_DATE + "}";
        assertEquals(expectedToString, transaction.toString());
    }
}
