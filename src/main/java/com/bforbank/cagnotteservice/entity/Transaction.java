package com.bforbank.cagnotteservice.entity;


import jakarta.persistence.*;
import lombok.Data;
import lombok.ToString;

import java.time.LocalDateTime;

@Entity
@Data
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Double amount;
    private LocalDateTime dateTransaction = LocalDateTime.now();

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id")
    private Customer customer;

    public Transaction(Double amount) {
        this.amount = amount;
    }
    public Transaction(Double amount,Customer customer) {
        this.amount = amount;
        this.customer = customer;
    }

    public Transaction() {}

    @Override
    public String toString() {
        return "Transaction{id=" + id + ", amount=" + amount + ", dateTransaction=" + dateTransaction + "}";
    }

}
