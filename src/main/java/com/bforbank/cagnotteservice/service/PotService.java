package com.bforbank.cagnotteservice.service;


import com.bforbank.cagnotteservice.entity.Customer;
import com.bforbank.cagnotteservice.entity.Transaction;
import com.bforbank.cagnotteservice.repository.CustomerRepository;
import com.bforbank.cagnotteservice.repository.TransactionRepository;
import org.springframework.stereotype.Service;
import com.bforbank.cagnotteservice.exception.EntityNotFoundException;


import java.util.List;

@Service
public class PotService {
    private final CustomerRepository customerRepository;
    private final TransactionRepository transactionRepository;

    private static final int MINIMUM_TRANSACTION_COUNT = 3;
    private static final double MINIMUM_BALANCE_REQUIRED = 10.0;

    public PotService(CustomerRepository customerRepository, TransactionRepository transactionRepository) {
        this.customerRepository = customerRepository;
        this.transactionRepository = transactionRepository;
    }

    public void addTransaction(Long customerId, Double amount) {
        if (amount <= 0) {
            throw new IllegalArgumentException("Transaction amount must be greater than zero\"");
        }

        Customer customer = customerRepository.findById(customerId).orElseThrow(() ->
                new EntityNotFoundException("Customer not found with ID: " + customerId));

        Transaction transaction = new Transaction();
        transaction.setCustomer(customer);
        transaction.setAmount(amount);
        transactionRepository.save(transaction);
    }

    public Double consulBalance(Long customerId) {
        customerRepository.findById(customerId).orElseThrow(() ->
                new EntityNotFoundException("Customer not found with ID: " + customerId));
        List<Transaction> transactions = transactionRepository.findAllByCustomerId(customerId);
        double balance = 0.0;
        for (Transaction transaction : transactions) {
            balance += transaction.getAmount();
        }
        return balance;
    }

    public boolean isPotAvailable(Long customerId) {
        customerRepository.findById(customerId).orElseThrow(() ->
                new EntityNotFoundException("Customer not found with ID: " + customerId));
        List<Transaction> transactions = transactionRepository.findAllByCustomerId(customerId);
        double balance = consulBalance(customerId);
        return !(transactions.size() < MINIMUM_TRANSACTION_COUNT || balance < MINIMUM_BALANCE_REQUIRED);
    }

    public Customer createCustomer(String name) {
        Customer customer = new Customer();
        customer.setName(name);
        return customerRepository.save(customer);
    }
}
