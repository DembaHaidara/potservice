package com.bforbank.cagnotteservice.controller;


import com.bforbank.cagnotteservice.entity.Customer;
import com.bforbank.cagnotteservice.service.PotService;
import lombok.Data;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/pots")
@RequiredArgsConstructor
public class PotController {

    private final PotService potService;

    @PostMapping("/add-transaction")
    public ResponseEntity<String> addTransaction(@RequestBody TransactionRequest request) {
        potService.addTransaction(request.getClientId(), request.getAmount());
        return ResponseEntity.ok().body("Transaction added successfully for the client.");
    }

    @GetMapping("/consult-balance/{clientId}")
    public ResponseEntity<String> consultBalance(@PathVariable Long clientId) {
        try {
            double balance = potService.consulBalance(clientId);
            return ResponseEntity.ok(String.valueOf(balance));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error while consulting the balance: " + e.getMessage());
        }
    }

    @GetMapping("/is-available/{clientId}")
    public ResponseEntity<String> isPotAvailable(@PathVariable Long clientId) {
        try {
            boolean isAvailable = potService.isPotAvailable(clientId);
            return ResponseEntity.ok(String.valueOf(isAvailable));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Error while checking the pot availability : " + e.getMessage());
        }
    }

    @PostMapping("/create-customer")
    public ResponseEntity<Customer> createCustomer(@RequestBody Customer request) {
        Customer customer = potService.createCustomer(request.getName());
        return ResponseEntity.status(HttpStatus.CREATED).body(customer);
    }

    @Data
    public static class TransactionRequest {
        private Long clientId;
        private Double amount;
    }

}