package com.bforbank.cagnotteservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CagnotteServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CagnotteServiceApplication.class, args);
	}

}
