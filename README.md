# PotService
- Test technique Java/SpringBoot
- Alternance -Dévelopeur backend Java

Bienvenue sur le repository du micro-service de gestion de cagnotte pour BforBank. Ce service permet aux commerçants d'ajouter de l'argent à la cagnotte d'un client à chaque passage en caisse, et aux clients de consulter le solde de leur cagnotte.

## Installation

Pour installer et exécuter ce projet :

1. Clonez ce repository sur votre machine locale.
2. Assurez-vous que Maven et Java 17 sont installés sur votre système.
3. Exécutez la commande suivante pour compiler le projet et démarrer le serveur :

```bash
mvn spring-boot:run
```
## Utilisation
Une fois le serveur démarré, vous pouvez accéder aux endpoints suivants via 
Postman ou tout autre outil de test d'API :

Pour créer un client :
```bash
POST http://localhost:8080/pots/create-customer
Content-Type: application/json

{
    "clientId": 1,
    "name": "demba"
}
```
Pour ajouter une transaction à la cagnotte d'un client :
```bash

POST http://localhost:8080/pots/add-transaction
Content-Type: application/json

{
"clientId": 1,
"amount": 100.0
}
```
Pour consulter la balance d'une cagnotte :
```bash
GET http://localhost:8080/pots/consult-balance/{customerId}

```
Pour vérifier si une cagnotte est disponible :

```bash
GET http://localhost:8080/pots/is-available/{customerId}
```
## Tests
```bash
mvn test
```
## Difficultés Rencontrées


- **Intégration de Swagger :** J'ai tenté d'intégrer Swagger pour documenter interactivement l'API, ce qui permettrait des tests facilités sans nécessiter des outils externes comme Postman. Bien que j'aie réussi à activer Swagger UI, je rencontre encore des difficultés car l'interface ne détecte pas correctement les endpoints dans le contrôleur. Les ressources Swagger sont maintenant localisées et disponibles, mais l'intégration n'est pas encore optimale.

## Auteur

**Haidara Demba**

- Email : demba.haidara50@gmail.com
- Site Web : https://dembahaidara.fr

